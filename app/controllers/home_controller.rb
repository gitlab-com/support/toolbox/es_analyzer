class HomeController < ApplicationController

  def index
    @elasticsearch = Home.new()
  end

  # Opens generated files in browser
  def status_file
    send_file "#{Rails.root}/es_output/es_status_check.json", :filename => "es_status_check.json", :disposition => 'inline', :type => "text/plain"
  end

  def whitespace_file
    send_file "#{Rails.root}/es_output/whitespace_tokenizer.json", :filename => "whitespace_tokenizer.json", :disposition => 'inline', :type => "text/plain"
  end

  def code_file
    send_file "#{Rails.root}/es_output/code_token_filter.json", :filename => "code_token_filter.json", :disposition => 'inline', :type => "text/plain"
  end

  def edgeNGram_file
    send_file "#{Rails.root}/es_output/edgeNGram_filter_token_filter.json", :filename => "edgeNGram_filter_token_filter.json", :disposition => 'inline', :type => "text/plain"
  end

  def lowercase_file
    send_file "#{Rails.root}/es_output/lowercase_token_filter.json", :filename => "lowercase_token_filter.json", :disposition => 'inline', :type => "text/plain"
  end

  def asciifolding_file
    send_file "#{Rails.root}/es_output/asciifolding_token_filter.json", :filename => "asciifolding_token_filter.json", :disposition => 'inline', :type => "text/plain"
  end

  def back_home
   redirect_to root_path
  end

  # Fires off when download file link is clicked

  def download_file
    directory_path  = "#{Rails.root}/es_output"
    timestamp       = Time.now.strftime "%Y-%m-%d-%s"
    es_zip_name     = "elasticsearch-#{timestamp}.zip"
    bundle_filename = "#{directory_path}/"+es_zip_name

    filenames = "es_output/*.json"
    %x{ cd #{directory_path}}
    %x{  zip -r  #{bundle_filename} #{filenames}}

    if File.exists?("#{Rails.root}/es_output/elasticsearch-#{timestamp}.zip")
      send_file "#{Rails.root}/es_output/elasticsearch-#{timestamp}.zip", :filename => "elasticsearch-#{timestamp}.zip", :disposition => "attachment",  :type => "application/zip"
    end
  end

# Checking Status of calls
  def check_status(response)
   if response != "200"
     @status = false
     #alert: "Something went wrong, check your input or try again."
   end
    if response == "200"
      @status = true
      #notice: "Please click the links below to see output."
      #flash[:notice] = "Please click the links below to see output."
    end
  end


  def create

    require 'net/http'
    require 'uri'
    require 'json'
    require 'csv'
    require 'zip'


    begin
      @elasticsearch = Home.new(form_params)

      es_url= @elasticsearch.esurl
      es_port= @elasticsearch.esport
      http_basic_username= @elasticsearch.httpbasicusername
      http_basic_password= @elasticsearch.httpbasicpassword
      text_content= @elasticsearch.textcontent
      search_text= @elasticsearch.searchtext

      text_content_uri = URI.parse("http://#{es_url}:#{es_port}/gitlab-production/_analyze?pretty")
      text_content_request = Net::HTTP::Get.new(text_content_uri)
      text_content_request.basic_auth(http_basic_username,http_basic_password)
      text_content_request.content_type = "application/json"
      text_content_request.body = JSON.dump({
        "tokenizer" => "whitespace",
        "filter" => ["code", "edgeNGram_filter", "lowercase", "asciifolding"],
        "explain" => true,
        "text" => text_content
      })

      text_content_req_options = {
        use_ssl: text_content_uri.scheme == "https",
      }

      text_content_response = Net::HTTP.start(text_content_uri.hostname, text_content_uri.port, text_content_req_options) do |http|
        http.request(text_content_request)
      end

      parsed = JSON.parse(text_content_response.body)
      puts "Text Content Analyze Response code: #{text_content_response.code}"

      search_term_uri = URI.parse("http://#{es_url}:#{es_port}/gitlab-production/_analyze?pretty")
      search_term_request = Net::HTTP::Get.new(search_term_uri)
      search_term_request.basic_auth(http_basic_username,http_basic_password)
      search_term_request.content_type = "application/json"
      search_term_request.body = JSON.dump({
        "analyzer" => "code_search_analyzer",
        "text" => search_text
      })

      search_term_req_options = {
        use_ssl: search_term_uri.scheme == "https",
      }

      search_term_response = Net::HTTP.start(search_term_uri.hostname, search_term_uri.port, search_term_req_options) do |http|
        http.request(search_term_request)
      end

      search_term_parsed = JSON.parse(search_term_response.body)
      puts "Search Term Analyze Response code: #{search_term_response.code}"

      code_search_analyzed_search_terms = []
      search_term_parsed["tokens"].each{|t| code_search_analyzed_search_terms << t["token"]}

      whitespaced_search_terms = search_text.split(' ')
      lowercase_search_terms   = search_text.downcase.split(' ')

      @token_explanation = {
        whitespace:{
          match: nil,
          order: 1
        },
        code:{
          match: nil,
          order: 2
        },
        edgeNGram_filter:{
          match: nil,
          order: 3
        },
        lowercase:{
          match: nil,
          order: 4
        },
        asciifolding:{
          match: nil,
          order: 5
        }
      }

      tokenizer_term_array = []
      token_filter_term_array = []

      File.open("es_output/#{parsed["detail"]["tokenizer"]["name"]}_tokenizer.json", "w") do |json|
        json << JSON.pretty_generate(parsed["detail"]["tokenizer"]["tokens"])
      end

      parsed["detail"]["tokenizer"]["tokens"].each do |t_object|
        whitespaced_search_terms.each do |term|

          if t_object["token"] == term
            tokenizer_term_array << term
          end
        end
      end

      if tokenizer_term_array.uniq.sort == whitespaced_search_terms.uniq.sort
        @token_explanation[parsed["detail"]["tokenizer"]["name"].to_sym][:match] = true
      else
        @token_explanation[parsed["detail"]["tokenizer"]["name"].to_sym][:match] = false
      end

      parsed["detail"]["tokenfilters"].each do |tokenfilter_object|
        File.open("es_output/#{tokenfilter_object["name"]}_token_filter.json", "w") do |json|
          json << JSON.pretty_generate(tokenfilter_object["tokens"])
        end

                  # If the tokenfilter_object name is lowercase or asciifolding
            # otherwise, we need to remove the lowercase or asciifolding
        if tokenfilter_object["name"] == "asciifolding"
          tokenfilter_object["tokens"].each do |t_object|  
            code_search_analyzed_search_terms.each do |term|
              if t_object["token"] == term
                token_filter_term_array << term
              end
            end
          end

          if token_filter_term_array.uniq.sort == code_search_analyzed_search_terms.sort
           
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = true
            token_filter_term_array = []
          else
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = false
            token_filter_term_array = []
          end
        elsif tokenfilter_object["name"] == "lowercase"
          tokenfilter_object["tokens"].each do |t_object|
            lowercase_search_terms.each do |term|
              if t_object["token"] == term
                token_filter_term_array << term
              end
            end
          end

          if token_filter_term_array.uniq.sort == lowercase_search_terms.sort
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = true
            token_filter_term_array = []
          else
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = false
            token_filter_term_array = []
          end
        else
          tokenfilter_object["tokens"].each do |t_object|
            whitespaced_search_terms.each do |term|
              if t_object["token"] == term
                token_filter_term_array << term
              end
            end
          end

          if token_filter_term_array.uniq.sort == whitespaced_search_terms.sort
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = true
            token_filter_term_array = []
          else
            @token_explanation[tokenfilter_object["name"].to_sym][:match] = false
            token_filter_term_array = []
          end
        end
      end

      #Get token explanation and populate file.
      File.open("es_output/token_report.json", "w") { |file| file.puts "#{JSON.pretty_generate(@token_explanation)}"}

      #Get common cluster stats and populate file.
      File.open("es_output/es_status_check.json", "w") { |file| file.puts "Cluster Health"}

      #Get cluster health
      uri         = URI.parse("http://#{es_url}:#{es_port}/_cat/health?v&pretty")
      uri_request = Net::HTTP::Get.new(uri)
      uri_request.basic_auth(http_basic_username,http_basic_password)
      uri_options = {
        use_ssl: uri.scheme == "https",
      }

      uri_response = Net::HTTP.start(uri.hostname, uri.port, uri_options) do |http|
        http.request(uri_request)
      end

      # Write Health to file
      File.open("es_output/es_status_check.json", "a") { |file| file.puts uri_response.body}
      File.open("es_output/es_status_check.json", "a") { |file| file.puts ""}
      File.open("es_output/es_status_check.json", "a") { |file| file.puts "Allocation"}

      # Get cluster allocation
      uri = URI.parse("http://#{es_url}:#{es_port}/_cat/allocation?v&pretty&h=shards,disk.indices,disk.used,disk.avail,disk.total,disk.percent,node")
      uri_request = Net::HTTP::Get.new(uri)
      uri_request.basic_auth(http_basic_username,http_basic_password)
      uri_options = {
        use_ssl: uri.scheme == "https",
      }

      uri_response = Net::HTTP.start(uri.hostname, uri.port, uri_options) do |http|
        http.request(uri_request)
      end
      # Write Health to file
      File.open("es_output/es_status_check.json", "a") { |file| file.puts uri_response.body}

      File.open("es_output/es_status_check.json", "a") { |file| file.puts ""}

      #Get cluster shards
      File.open("es_output/es_status_check.json", "a") { |file| file.puts "Shards"}
      uri = URI.parse("http://#{es_url}:#{es_port}/_cat/shards?v&pretty&h=index,shard,prirep,state,docs,store,node")
      uri_request = Net::HTTP::Get.new(uri)
      uri_request.basic_auth(http_basic_username,http_basic_password)
      uri_options = {
        use_ssl: uri.scheme == "https",
      }

      uri_response = Net::HTTP.start(uri.hostname, uri.port, uri_options) do |http|
        http.request(uri_request)
      end
      #Write to file
      File.open("es_output/es_status_check.json", "a") { |file| file.puts uri_response.body}

    sleep 1
    redirect_to output_path(token_explanation: @token_explanation, code_search_analyzed_search_terms: code_search_analyzed_search_terms)

    rescue Exception => e
      flash[:danger] = e
      Rails.logger.info e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")

      redirect_to root_path
    end
  end

  def output
    token_params       = params.require(:token_explanation).permit!
    @token_explanation = token_params.to_h.sort_by { |k, v| v[:order] }
    @code_search_analyzed_search_terms = params[:code_search_analyzed_search_terms]
    
    @info         = File.read("#{Rails.root}/app/assets/markdown/info.html.md")
    @whitespace   = File.read("#{Rails.root}/app/assets/markdown/whitespace.html.md")
    @code         = File.read("#{Rails.root}/app/assets/markdown/code.html.md")
    @edgeNGram    = File.read("#{Rails.root}/app/assets/markdown/edgeNGram.html.md")
    @lowercase    = File.read("#{Rails.root}/app/assets/markdown/lowercase.html.md")
    @asciifolding = File.read("#{Rails.root}/app/assets/markdown/asciifolding.html.md")
    
  end

  private

  def form_params
    params.require(:home).permit(:esurl,:esport,:httpbasicusername,:httpbasicpassword,:searchtext,:textcontent)
  end
end
