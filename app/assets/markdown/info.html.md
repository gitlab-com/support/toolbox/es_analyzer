####Tokenizer/Token Filter Descriptions

---

Click the Tokenizer or filters to the left to see each token that was found.
You can also click the question mark for more details on the Tokenizer or Token filter.


**GitLab Documentation**
[Elasticsearch Knowledge Documentation](https://docs.gitlab.com/ee/development/elasticsearch.html)
[Elasticsearch Integration Documentation](https://docs.gitlab.com/ee/integration/elasticsearch.html)

**Elasticsearch Documentation**
[Tokenizer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-tokenizers.html)
[Token Filter](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-tokenfilters.html)
