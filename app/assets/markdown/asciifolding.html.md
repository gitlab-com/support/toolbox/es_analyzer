####ASCIIFOLDING

We use the **[ASCII Folding Token Filter](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-asciifolding-tokenfilter.html)** as one of four Token Filters in `code_analyzer`.

```
curl -X GET "es_endpoint/_analyze" -H 'Content-Type: application/json' -d '{
  "tokenizer" : "whitespace",
  "filter" : ["asciifolding"],
  "text": "açaí à la carte"
}'
```

It converts alphabetic, numeric, and symbolic characters that are not in the Basic Latin Unicode block (first 127 ASCII characters) to their ASCII equivalent, if one exists.

**Example:**
The tokens `["açaí" "à", "la", "carte"]` are converted to `[ acai, a, la, carte ]`.