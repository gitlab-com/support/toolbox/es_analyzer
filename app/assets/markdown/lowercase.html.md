####Lowercase

We use the **[Lowercase Token Filter](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lowercase-tokenfilter.html)** as one of four Token Filters in `code_analyzer`.

```
curl -X GET "es_endpoint/_analyze" -H 'Content-Type: application/json' -d '{
  "tokenizer" : "whitespace",
  "filter" : ["lowercase"],
  "text": "THE Lazy DoG"
}'
```

This filter simply changes token text to lowercase.

**Example:**
The text `[THE, Lazy, DoG]` will lowercase token text to `[the, lazy, dog]`.