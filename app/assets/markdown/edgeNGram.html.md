####edgeNGram

[Edge NGram](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-edgengram-tokenizer.html) token filter allows inputs with only parts of a token to find the token.

```
    edgeNGram_filter: {
      type: 'edgeNGram',
      min_gram: 2,
      max_gram: 40
    }
```

For example it would turn `glasses` into permutations starting with `gl` and ending with `glasses`, which would allow a search for `glass` to find the original token `glasses`.