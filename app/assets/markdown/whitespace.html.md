####Whitespace

We use the **[Whitespace Tokenizer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-whitespace-tokenizer.html)** as the Tokenizer for our custom analyzer `code_analyzer`.

```
curl -X POST "es_endpoint_analyze" -H 'Content-Type: application/json' -d' {
  "tokenizer":
  "whitespace",
  "text": "The 2 QUICK Brown-Foxes jumped over the lazy dog's bone."
}'
```

It will break text into terms whenever it encounters a whitespace character.

The text:
`The 2 QUICK Brown-Foxes jumped over the lazy dog's bone.`

Breaks on each whitespace and creates the terms:
`[ The, 2, QUICK, Brown-Foxes, jumped, over, the, lazy, dog's, bone. ]`