####Code

The custom `code` Token Filter is a Pattern Capture that splits tokens into more easily searched versions of themselves.

The Patterns and Settings are used as follows:
    
**Capture CamelCased and lowedCameCased strings as separate tokens.**
Ex: likeThis one tooCan => [likeThis, one, tooCan]
`(\\p{Ll}+|\\p{Lu}\\p{Ll}+|\\p{Lu}+)`

**Capture digits as separate tokens.**
Ex: like this 2 => [like, this, 2]
`(\\d+)`

**Capture CamelCased strings recursively.**
Ex: ThisIsATest => [ThisIsATest, IsATest, ATest, Test]
`(?=([\\p{Lu}]+[\\p{L}]+))`

**Capture terms inside double-quotes.**
Ex: "like this" => [like this]
`((?:\\"|[^"]|\\")*)`

**Capture terms inside single-quotes.**
Ex: 'like this' => [like this]
`((?:\\'|[^']|\\')*)`

**Capture terms seperated with periods in-between.**
Ex: like.this.one => [like, this, one]
`'\.([^.]+)(?=\.|\s|\Z)'`

**Capture terms on separating by paths slashes.**
Ex: like/this/one => [like, this, one]
`'\/?([^\/]+)(?=\/|\b)'`