# GitLab Elasticsearch Analyzer!

The GitLab Elasticsearch Analyzer!  outputs the tokens that are created for the [code_analyzer](https://docs.gitlab.com/ee/development/elasticsearch.html#code_analyzer)
so that users can figure out why their search term did not return any results for file content.  
In the future this tool could fully simulate the Elasticsearch search function that is in the GitLab UI when Advanced search is enabled.  

In Version 1, we are only using the `code_analyzer`. We hope to add more in the future.

## Why did we build this tool?
GitLab support recognized a problem with disseminating searches that were not returning results.

An [issue created by @blunceford](https://gitlab.com/gitlab-org/gitlab/issues/10693) demonstrates this common problem specifically with file names and content.  
Using the the GitLab Elasticsearch Analyzer!, Support will be able to cut troubleshooting time down and identify the exact reason why a search did not return results.

## How to run the GitLab Elasticsearch Analyzer!
Running this tool is simple and can be ran three ways.
 - [Using Docker Compose](#using-docker-compose)
 - [Using Docker](#using-docker)
 - [Cloning the repo](#cloning-the-repo)

>This [tool's Docker image](https://gitlab.com/gitlab-com/support/toolbox/es_analyzer/container_registry) and the [Elasticsearch database Docker image](https://gitlab.com/gitlab-com/support/toolbox/es_analyzer_data/container_registry) that it can use are hosted in each projects respective Registry.

### Using Docker Compose
The recommened way to run the GitLab Elasticsearch Analyzer! is by using Docker Compose.  
The Docker Compose file consists of two containers in a single network, this project and an [OSS version of Elasticsearch 6.3.2](https://gitlab.com/gitlab-com/support/toolbox/es_analyzer_data).  
The GitLab Elasticsearch Analyzer! resides at `172.18.04:3000` and the Elasticseatch database is at `172.18.0.2:9200`.

Using this method, you do not have to worry about having an Elasticsearch database nor a GitLab instance to troubleshoot searching.

To run, simply clone the repo and run Docker Compose:

```bash
git clone git@gitlab.com:gitlab-com/support/toolbox/es_analyzer.git
docker-compose up
```

>The GitLab Elasticsearch Analyzer! and Elasticsearch container's default ports are `3000` and `9200` respectively. If you already have something using these ports, changes can be made in the `.env` file to what you need.   
*For example: To reach the tool's UI on port 3001, change the `GITLAB_PORT` env to 3001.*

#### Using Docker
You can use the GitLab Elasticsearch Analyzer! container by it's self to connect your GitLab instance that has search enabled with Elasticsearch.

To run the GitLab Elasticsearch Analyzer! Docker container:

``` bash
docker run -itP \
--name es_analyzer \
-p 3000:3000 \
registry.gitlab.com/gitlab-com/support/toolbox/es_analyzer
```

### Cloning the repo

* Dependencies:
   * Ruby `2.6.3`
   * Rails `5.2.4.2`

After cloning the repo, run:

```bash
bundle install
rails server
bin/rails db:migrate RAILS_ENV=development
```

---

### Opening the tool

After starting, open your browser to http://localhost:3000.

----

## Gotcha's

**At first run, you will need to run migrations.**

When running a repo based install:

```bash
bin/rails db:migrate RAILS_ENV=development
```

Running in a Docker based installation (migrations are automatically ran on start-up, but adding for reference):

```bash
docker exec -d es_analyzer bin/rails db:migrate RAILS_ENV=development
```

**On some Elasticsearch instances, the [Elasticsearch container](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html) may not be able to start if `vm.max_map_count` is set too low.**

```bash
elasticsearch    | ERROR: [1] bootstrap checks failed
elasticsearch    | [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
```

If this happens on the start of an Elasticsearch container, you will need to increase the `max_map_count` by changing this setting. How to change this depends on your OS and how Docker is installed.  

Some examples:  

_**Linux**_  

```bash
sysctl -w vm.max_map_count=262144`
```

_**Docker Desktop on MacOS: (don't forget to kill the screen session once done!)**_

```bash
screen ~/Library/Containers/com.docker.docker/Data/vms/0/tty
sysctl -w vm.max_map_count=262144
```

_**Docker Machine**_

```bash
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
```
----

## How to use:

After following the [How to Run Guide](#how-to-run-the-gitlab-elasticsearch-analyzer), you are shown the tools main page.

In the screenshot below, the Elasticsearch URL and port have been filled in automatically to point to the Elasticsearch container being used in the Docker Compose file. If you are using your own Elasticsearch instance, enter your URL and port. If Basic Auth is being used, you will need to enter those also.

![index_page](app/assets/images/index.png)

In the `TEXT TO SEARCH FOR` field, enter the search term that would be entered in the GitLab UI search bar.

Then fill the `TEXT TO SEARCH AGAINST` field with an example of the code snippet or file snippet that the user expects the `TEXT TO SEARCH FOR` term to return.

In our example, we are searching `Licroloft.Extensions.Logging.Abstractions` which we would expect to match and return the code snippet of:

```
<PackageReference Include="Licroloft.Extensions.Logging.Abstractions" Version="3.1.0" />
```

Now click `GET DATA`.

After clicking, you are shown the Output page.

![index_page](app/assets/images/output.png)

On this page, you are give the Tokenizer and token filter checklist. If a token was matched, you are given a green checkmark. If not, a red 'X' is shown.

This shows us where a match was either found or was not found when the search term was compared to the search text after tokens were made by the Tokenizer and Token filters.

In the above example, we see that the search failed on the `whitespace` tokenizer, succeeded on the `code` token filter, and failed on the `edgeNGram_filter` token filter.

We also see a that the search in the GitLab UI would also fail and an information flash about the `edgeNGram_filter` and a common reason for its failure to match.

To view which tokens were created from the Tokenizer and token filters, you can click on the name of the Tokenizer or filter.

For example, if you click on the word `edgeNGram_filter`, another window will open so that we can view the tokens that were created.

![index_page](app/assets/images/tokenreturn.png)

Looking through the `edgeNGram_filter's` tokens, we see the token closest to match our search term is `Licroloft.Extensions.Logging.Abstraction`. Which does not match our search term of `Licroloft.Extensions.Logging.Abstractions`. This is due to the `max_gram` setting of the `edgeNGram_filter` being set to 40. Which means that once token size hits 40 characters, the `edgeNGram_filter` moves on to the next edgeNGram token.

In summary, the GitLab Elasticsearch Analyzer! has identified that our search was not returned due to `max_gram` of 40 being too small for our search term. This enables us to bring a full example with data to GitLab developers to improve our Elasticsearch implementations accurately.

To see what all other filters returned, simply click on its name. To see more information about the Tokenizer and filters, click the `?` next to its respective name. You can also, check the status of your Elasticsearch cluster and download a copy of all tokens that were returned.

Most times, the filter you need to research is the first filter to fail after a passing filter since the Tokenizer and filters work sequentially or top to bottom. Which is why we focused on the `edgeNGram_filter` in the above example.

----

## How to report search issues you find

Create an issue in [GitLab Elasticsearch Analyzer Reports](https://gitlab.com/m_lussier/gitlab-elasticsearch-analyzer-reports/-/issues) to coalesce GitLab Elasticsearch Analyzer! results into our explanation of potential improvements.

(Example below is the default issue template used in GitLab Elasticsearch Analyzer Reports)

```
<!---
Run the GitLab Elasticsearch Analyzer! tool (Follow the [README](https://gitlab.com/ehenley/es_analyzer#gitlab-elasticsearch-analyzer) for instruction)

Download and Attach `elasticsearch-<date>.zip`
--->

##### Search Terms
(Text being entered into the GitLab UI search bar)

##### Text Content
(Text that is expected to be returned in search results)

##### Problem Tokenizer & Token Filter/s
(List of each tokenizer/token filter that failed and a list of token/s closest to matching)

##### Reason for Failure
(Reason why tokenizer/token filter failed)

##### Possible Solution
(A detailed explanation/theory/idea of how it could match)
```


----


## Development and Contributing

Bug reports and merge requests are welcome! Please make sure to follow GitLab's [Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)
and adhere to our [Values](https://about.gitlab.com/handbook/values/). Live long and prosper! :spock:

To develop:  
You will need to have a GitLab instance with Elasticsearch enabled or have pulled the [OSS Elasticearch container](https://gitlab.com/gitlab-com/support/toolbox/es_analyzer_data) that this project uses.
After cloning the project, run:

```bash
bundle install
rails sevrer
```