class CreateHomes < ActiveRecord::Migration[5.2]
  def change
    create_table :homes do |t|
      t.string :escurl
      t.string :esport
      t.string :httpbasicusername
      t.string :httpbasicpassword
      t.text :searchtext
      t.text :textcontent

      t.timestamps
    end
  end
end
