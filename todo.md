Can we get one more week to future proof it and make analyzers/tokenizers/token_filters seletable from the current ES instances analyzers/tokenizers/token_filters?

Priority:
*  Document example mode

√ *  Re-order tokenizer/token filters and adding failure examples for each

√ *  Generate GitLab install that can give ES so that it can be used with ES analyzer
√*  (If possible) Get ES IP and put it into ES analyzer if easy (less than 2 hours) (otherwise, just make it easy to access)

Priority:
√ - Add display this hash for text content, also add (confirm with Lee) a text box below this that is a case statement with common "why this failed" that can grow in the future i.e 

        - Tokenizer Output -
        X - whitespace
        (example of this tokenizer/token_filter: "foo bar" is now ["foo", "bar"] )


        - Token Filter output -
        √ - code
        X - edgeNGram_filter
        X - lowercase
        X - asciifolding

        - Common Reason For Failure -
        edgeNGram_filter currently has `min_gram: 2` and `max_gram: 40` commonly we see tokens not be found here because they are too long, for example:

          once the token "Microsoft.Extensions.Logging.Abstractions" reaches edgeNGram_filter the `max_gram 40` cuts off the "s" in "Microsoft.Extensions.Logging.Abstractions" and it does not show up in searches for "Microsoft.Extensions.Logging.Abstractions"



* Documentation pass for es analyzer
  - Add documentation
      * NOTES on documentation
      - Section Titles (to inform descriptions)
        Why did we build this tool
        How to install
        How to use+What are the outputs you will see
        Where to update information in gitlab issues (what tags what group is responsible for es)
        Link in our docs to this tool 

    

√ Get 1:1 back towards a normal schedule  

* Write Tests
      L: Some basic ones, sure, but otherwise, lets see how long it lives and we’ll go from there.
    
* Continue refactor
  √ - Finish refactor in solo script
  √ - Place refactor changes into Eldridge Rails app

  
√ Add the ability for new text boxes to appear when clicking on tokenizer/token filters for explanation
√ Add date to zip of files for historical data

  * - Add logic for http basic auth (Note: we weren't figuer out nginx setup for ES clusters yet, but we have the same auth setup in our script that we used in a previous setup that had http auth)
  * Add file that is text version of rails app for future use
  * Add fuzzy matching and tokens that do match to output

  



    
    √ - Improve output of ES information in Rails
      * NOTES on Improve output of ES information in Rails
        (list all filters and say "√" or "X" where token found)

        X SEARCH TOKEN/S NOT FOUND FAILURE 
        - Tokenizer Output -
        X - whitespace
        (example of this tokenizer/token_filter: "foo bar" is now ["foo", "bar"] )


        - Filter output -
        √ - code
        X - edgeNGram_filter
        X - lowercase
        X - asciifolding


        √ SEARCH TOKEN/S FOUND SUCCESS
        - Tokenizer Output -
        √ - whitespace

        - Filter output -
        √ - code
        √ - edgeNGram_filter
        √ - lowercase
        √ - asciifolding

        (Improve feedback on success and failure of notifications)

    √ - Put it in a docker container