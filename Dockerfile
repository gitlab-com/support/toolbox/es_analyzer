FROM ruby:2.6.3
MAINTAINER GitLab

RUN apt-get update && apt-get install -y \
  build-essential \
  nodejs \
  zip
RUN mkdir -p /es_analyze
WORKDIR /es_analyze

# Copy the Gemfile as well as the Gemfile.lock and install
# the RubyGems. This is a separate step so the dependencies
# will be cached unless changes to one of those two files
# are made.
COPY Gemfile Gemfile.lock ./

RUN gem install bundler && bundle install --jobs 20 --retry 5

# Copy the main application.
COPY . ./

# Expose port 3000 to the Docker host, so we can access it
# from the outside.
EXPOSE 3000

ENTRYPOINT ["/es_analyze/migrations"]

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
