Rails.application.routes.draw do
  #get 'home/index'

  root 'home#index'

  resources :home

# links
  get 'status' => 'home#status_file'
  get 'whitespace' => 'home#whitespace_file'
  get 'code' => 'home#code_file'
  get 'edgeNGram_filter' => 'home#edgeNGram_file'
  get 'lowercase' => 'home#lowercase_file'
  get 'asciifolding' => 'home#asciifolding_file'
  get 'download_file' => 'home#download_file'

# Output page
  get 'output' => "home#output"

#Back to index page

  root :controller => 'static', :action => '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
